import java.util.Scanner;

public class Bonusaufgabe {
    public static void main(String[] args) {

        Scanner scan2 = new Scanner(System.in);
        System.out.println("Bitte gebe die gewünschte Höhe des Baumes ein: ");
        int hoehe = scan2.nextInt();


        for (int i = 1; i<=hoehe; i++) {

            for (int j = 1; j <= hoehe-i; j++)
                System.out.print(" ");

            for (int j = 1; j <= (2*i-1); j++)
                System.out.print("*");


            System.out.println();
        }

        for (int i=1; i<hoehe; i++)
            System.out.print(" ");
        System.out.println("I");
    }
}
